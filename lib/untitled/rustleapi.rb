require 'socket'

module Rustle
  def self.exec_run(ip, file)
    socket = ::TCPSocket.new ip, 7568
    socket.puts 'getauth'
    auth = socket.gets.chomp
    socket.puts "auth #{auth.reverse}"
    success = socket.gets.chomp == 'success'
    unless success
      puts "Cannot auth to Rustle at #{ip}:7568"
      return false
    end
    socket.puts "run #{file}"
    socket.close
    true
  end
end
