# frozen_string_literal: true

require "untitled/version"
require "socket"
require "untitled/messages"

module Untitled
  class Error < StandardError; end

  class Listener
    attr_accessor \
      :clients,
      :commander,
      :users,
      :channels

    def initialize(commander)
      @clients = []
      @commander = commander
      @users = {}
      @channels = {}
      @commander.register 'USER', self.method(:handle_user)
      @commander.register 'JOIN', self.method(:handle_join)
      @commander.register 'NICK', self.method(:handle_nick)
      @commander.register 'PRIVMSG', self.method(:handle_privmsg)
      @commander.register 'PART', self.method(:handle_part)
      @commander.register 'LIST', self.method(:handle_list)
    end

    def start
      ::TCPServer.open 6667 do |server|
        puts "Started server at 6667"
        loop do
          Thread.start server.accept do |client|
            loop do
              input = client.gets
              next if input.nil?
              str = input.chomp
              components = str.split ' ', 2
              @users[client.addr[3]] = {} if @users[client.addr[3]].nil?
              @users[client.addr[3]][:client] = client
              puts "<= #{getstr(client)} #{str}"
              unless commander.pass components[0], components[1], self, client
                clients.each do |c|
                  c.puts str
                end
              end
            end
          end
        end
      end
    end

    def getstr(client)
      "#{@users[client.addr[3]][:username]}|#{client.addr[3]}"
    end

    def reply(client, msg)
      client.puts msg
      puts "=> #{getstr client} #{msg}"
    end

    def broadcast(msg, condition: [])
      clients.each do |c|
        continue = true
        condition.each do |i|
          a = i.split ' '
          case a[0]
          when 'in'
            continue = false unless @channels[a[1]][:users].include?(c.addr[3])
          else
            puts "Invalid condition: #{i}"
          end
        end
        next unless continue
        reply c, msg
      end
    end

    def add_user_to_channel(channel, client, topic: 'No topic')
      @channels[channel] = {
          clients: [],
          topic: topic
      } if @channels[channel].nil?
      @channels[channel][:clients] += [client]
      @channels[channel]
    end
  end

  class Commander
    attr_accessor :registered, :triggers

    def initialize
      @registered = {}
      @triggers = {}
    end

    def register(name, function)
      @registered[name] = function
    end

    def register_trigger(name, function)
      @triggers[name] = function
    end

    def register_sub_trigger(parent, name, function)
      @triggers["#{parent}.SUBTRIGGER{{#{name}}}"] = function
    end

    def trigger(name, arg = '', sub = '~', client_messenger)
      if sub == '~'
        @triggers[name].call arg, client_messenger unless @triggers[name].nil?
        not @triggers[name].nil?
      else
        @triggers["#{name}.SUBTRIGGER{{#{sub}}}"].call arg, client_messenger unless @triggers["#{name}.SUBTRIGGER{{#{sub}}}"].nil?
        not @triggers["#{name}.SUBTRIGGER{{#{sub}}}"].nil?
      end
    end

    def pass(name, args, listener, client)
      @registered[name].call listener, client, args unless @registered[name].nil?
      not @registered[name].nil?
    end
  end

  class Messenger
    attr_accessor :client

    def initialize(client)
      @client = client
    end

    def send_message(channel, from, message)
      @client.puts ":#{from} PRIVMSG #{channel} :#{message}"
      puts "-- #{from}|localhost sent '#{message}' to channel #{channel}"
    end
  end
end
